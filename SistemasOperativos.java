package com.company;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import javax.swing.*;


public class SistemasOperativos extends JFrame implements ActionListener {

    //creamos algunas variables que necesitamos
    public Panel panel;                                             // tipo panel
    JLabel cuadro1;                                                 // tipo JLabel o etiqueta
    JLabel cuadro2;
    JLabel cuadro3;
    JLabel cuadro4;
    //JTextArea [] area= new JTextArea[5];
    JButton boton;                                                 // tipo botonm
    JTextArea area1,area2;                                         // tipo area



    public SistemasOperativos() {

        setSize(1200, 800);
        // setResizable(true);
        setVisible(true);
        panel = new Panel();
        panel.setLayout(null);
        this.setContentPane(panel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Compilador");
        panel.setVisible(true);
        setSize(1200, 700);
        setDefaultCloseOperation(EXIT_ON_CLOSE);                            // en esta parte del codigo generamos la ventana, asignamos tamaño u algunas operaciones como
                                                                            //como cerrar y el titulo

        cuadro1 = new JLabel();
        cuadro2 = new JLabel();
        cuadro3 = new JLabel();
        cuadro4 = new JLabel();
        boton = new JButton();                                              //creamos el boton
        area1 = new JTextArea();                                             //creamos el area de texto


        // asignamos tamaño y posicion
        cuadro1.setBounds(0, 0, 450, 350);                    //.setbound(posicion en X,  posicion en Y,  longitud en X,  longitud en Y)
        cuadro2.setBounds(450, 0, 750, 350);                  //asi funciona el setbounds
        cuadro3.setBounds(0, 350, 450, 350);
        cuadro4.setBounds(450, 350, 750, 350);
        area1.setBounds(0, 0, 450, 350);
        boton.setBounds(380, 330, 70, 20);

        //asignamos color
        cuadro1.setBackground(Color.black);
        cuadro2.setBackground(Color.PINK );
        cuadro3.setBackground(Color.magenta);
        cuadro4.setBackground(Color.white);
        area1.setBackground(Color.black);
        area1.setForeground(Color.white);
        boton.setBackground(Color.black);

        // hacemos opacos los cuadros para que se pueda distinguir el colo
        cuadro1.setOpaque(true);
        cuadro2.setOpaque(true);
        cuadro3.setOpaque(true);
        cuadro4.setOpaque(true);

        // agregamos al panel los objetos creados
        add(boton);
        add(cuadro1);
        add(cuadro2);
        add(cuadro3);
        add(cuadro4);
        add(area1);

        boton.addActionListener(this);


          /*Metodo donde generamos el panel, asignamos tamaño, y otras especificaciones de estem
    se crean los cuadros, se asignan tamaños asi como el color,
    se crea el area de texto, se asigna tamaño y color a la letra,
    se crea el boton (aunque esta casi imperceptible), se le asigna el color y se agrega el eventi addlitener
    */

    } //fin de clase Draw


    public static void main(String[] args) {
        // se crea el objeto de la clase sistemas operativos y se ejecuta
        SistemasOperativos op = new SistemasOperativos();
    }

    public void paint(Graphics g, int x, int y){

        Graphics2D circulo1=(Graphics2D)g;                              // se genera un objeto de la clase Graphics
        circulo1.setStroke(new BasicStroke(10.f));                  // se le da un tipo de borde al circulo
        circulo1.setPaint(Color.red);                                    // se agrega color
        circulo1.fillOval(x, y, 90, 90);                         // se asigna posicion y tamaño (tamaño en x, tamaño en y, posicion en x, posicione en y );


        area2 = new JTextArea();                                           // se crea el objeto area2
        area2.setBounds(500,250,60,60);                      // se asigna posicion y tamaño
        area2.setText("Inicio");                                           // se agrega una etiqueta de texto
        add(area2);                                                        //se agrega al panel

    }


    @Override
    public void actionPerformed(ActionEvent ae) {

        if(ae.getSource()==boton)                                       //escuchamos si se presiona el boton o no
             if(area1.getText().equals("Run") ){                        // comparamos lo escrito en el area de texto

                 paint(panel.getGraphics(),470,250);              // mandamos llamar al metodo para dibujar el circulo y asignamos posicion
                 paint(panel.getGraphics(),580,50);
                 paint(panel.getGraphics(),750,250); //220
                 paint(panel.getGraphics(),990,50);
                 paint(panel.getGraphics(),1070,250);


             }

    }
}
